###study drupal7.53###

##### Modules #####
1. admin_menu
2. colorbox-7.x-2.12
3. devel-7.x
4. jquery_update-7.x-2.7
5. libraries-7.x-2.3
6. module_filter-7.x-2.0
7. omega-7.x-4.4


---

#### 2017-01-08 ####

1. 学习了omega子主题中创建layouts，创建好layouts之后，在`配置->开发->性能`中清空缓存按钮

#####参考#####
1. [Creating A Custom Layout](https://www.drupal.org/node/1936980),创建一个自定义的布局
2. [Adding a new region to your Omega 4.0 subtheme (Drupal)](http://www.drupalwoo.com/content/blog/adding-new-region-your-omega-40-subtheme-drupal),是建立布局的教程，在里面明白，创建完布局之后，需要`清空缓存`
3. [drupal7 如何清空缓存？](https://zhidao.baidu.com/question/649460040321685885.html),清除在`配置->开发->性能`清空缓存


