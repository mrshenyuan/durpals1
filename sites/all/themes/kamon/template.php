<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
/**
 * Implements template_preprocess_html().
 *
 * The changes made in this function affect the variables for the html.tpl.php
 * template file, which is located in templates/html.tpl.php of this theme.
 * Using drupal_css_css() is covered on pages 344-345.
 */
function kamon_preprocess_html(&$vars) {


    // Create a variable containing the path to the theme. We'll use this in
    // in html.tpl.php.
    $vars['path'] = drupal_get_path('theme', 'kamon');

    // Add a conditional stylesheet for Internet Explorer.
    // The "browsers" key allows you to specify what versions of IE to target. In
    // this case we are targeting "less than or equal to IE 7". Drupal will wrap
    // the code for this stylesheet in conditional comments:
    // "<!--[if lte IE 7]> … <![endif]-->. For more details about conditional
    // comments see: http://www.quirksmode.org/css/condcom.html
    drupal_add_css($vars['path'] . '/css/ie.css', array(
            'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE),
            'group' => CSS_THEME,
            'preprocess' => FALSE,
        )
    );

    $vars['classes_array'][] = 'kamon';
    // In template_preprocess_html() we have easy access to the "class" attribute
    // for the <body> element. Here we add a helper class that indicates that
    // there is no title for the page to help styling the content region easily.
//    if (!drupal_get_title()) {
//        $vars['classes_array'][] = 'no-title';
//    }
}

