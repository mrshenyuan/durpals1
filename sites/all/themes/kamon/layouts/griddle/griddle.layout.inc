name = Griddle
description = A Layout for kamon website
preview = preview.png
template = griddle-layout

; Regions
regions[branding]       = Branding
regions[slidershow]     = Slider Show
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[content_after] = Content - After

; Stylesheets
stylesheets[all][] = css/layouts/griddle/griddle.layout.css
stylesheets[all][] = css/global.css
