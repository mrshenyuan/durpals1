<!--.mobile_menu_wrapper start-->
<div class="mobile_menu_wrapper">
  <header class="brand-heading" href="<?php print $front_page; ?>">
    <div class="tk-grippie"></div>
  </header>
</div>
<!--/.mobile_menu_wrapper end-->
<!--.desktop_top_wrapper start-->
<div class="desktop_top_wrapper">
  <a href="<?php print $front_page; ?>" class="brand-heading"></a>
</div>
<!--/.desktop_top_wrapper end-->
<div class="drupalstuff-top">

</div>
<div class="main-container">
  <?php print render($page['content']); ?>
  <?php print render($page['slidershow']); ?>
  <?php print render($page['content_after']); ?>
</div>

<div class="drupalstuff"></div>

<div<?php print $attributes; ?>>
  <header class="l-header" role="banner">
    <div class="l-branding">


      <?php print render($page['branding']); ?>
    </div>

    <?php print render($page['header']); ?>
    <?php print render($page['navigation']); ?>
  </header>

  <div class="l-main">
    <div class="l-content" role="main">
      <?php print render($page['highlighted']); ?>
      <?php print $breadcrumb; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <?php print $feed_icons; ?>
    </div>

  </div>

</div>
